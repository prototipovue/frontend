// =========================================================
// * Vuetify Material Dashboard - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import vueDebounce from 'vue-debounce'
import VuetifyConfirm from 'vuetify-confirm'
import moment from 'moment'

require('./plugins/axios')
require('./plugins/toastification')

Vue.use(vueDebounce, {
  defaultTime: '500ms',
})

Vue.use(VuetifyConfirm, {
  vuetify,
  buttonTrueText: 'Aceptar',
  buttonFalseText: 'Cancelar',
  color: 'danger',
  title: 'Confirmar acción',
  width: 350,
  property: '$confirm',
})

Vue.filter('toCurrency', function (value) {
    if (typeof value !== 'number') {
        return value
    }
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0,
    })
    return formatter.format(value)
})

Vue.filter('dateUser', function (date) {
  return moment(date).format('DD/MM/YYYY')
})

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (localStorage.getItem('token') === null && to.name !== 'Login') {
    next({ name: 'Login' })
  } else if (localStorage.getItem('token') !== null && to.name === 'Login') {
    next({ name: 'Dashboard' })
  } else {
    next()
  }
})

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app')

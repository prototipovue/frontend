import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'Login',
      path: '/login',
      component: () => import('@/views/dashboard/login/Index'),
    },
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'Facturas',
          title: 'Facturas',
          path: '/invoices',
          component: () => import('@/views/dashboard/pages/invoices/Index'),
        },
        {
          name: 'Usuarios',
          title: 'Usuarios',
          path: 'settings/users',
          component: () => import('@/views/dashboard/pages/users/Index'),
        },
        {
          name: 'Productos',
          path: 'settings/products',
          component: () => import('@/views/dashboard/pages/products/Index'),
        },
        {
          name: 'Tipos de Productos',
          path: 'settings/product-types',
          component: () => import('@/views/dashboard/pages/productTypes/Index'),
        },
      ],
    },
  ],
})

import Vue from 'vue'
import axios from 'axios'

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
const schema = process.env.VUE_APP_HTTP_SCHEMA

axios.defaults.baseURL = schema + process.env.VUE_APP_API_BASE_URL

axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

// const token = localStorage.getItem('token')

// if (token) {
//   const AUTHORIZATION = 'Authorization'
//   axios.defaults.headers.common[AUTHORIZATION] = token
// }

Vue.prototype.$axios = axios

import Vue from 'vue'
import Toast from 'vue-toastification'
import 'vue-toastification/dist/index.css'

const options = {
    timeout: 1500,
}

Vue.use(Toast, options)
